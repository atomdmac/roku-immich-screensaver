Sub RunScreenSaver()
  m.port = createObject("roMessagePort")

  screen = createObject("roSGScreen")
  screen.setMessagePort(m.port)

  scene = screen.createScene("ImmichScreensaver")

  screen.show()

  scene.setField("isReady", "true")

  while true
    msg = wait(5000, m.port)
    if type(msg) <> invalid then
      print "top::msg: "; msg
    end if
  end while
end sub

