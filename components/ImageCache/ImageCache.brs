Function init()
  print "ImageCache::init"

  m.urlHandler = CreateObject("roSGNode", "Content UrlHandler")
  m.urlHandler.observeField("response", "handleImageResponse")

  m.cache = createObject("roArray", 0, true)
  m.top.setField("cache", m.cache)

  m.top.observeField("assets", "handleSetAssets")
  m.top.observeField("currentImageId", "handleSetCurrentImageId")
EndFunction

Function loadAsset(assetId$)
  print "ImageCache::loadAsset"

  context = createObject("roSGNode", "Node")

  params = {
    headers: {
      "x-api-key": m.top.getField("apiKey")
    },
    uri: m.top.getField("baseAssetUrl") + "/" + assetId$ + "/thumbnail?size=preview",
    method: "GET",
    params: {}
    file: "tmp:/" + assetId$
    assetId: assetId$
  }

  context.addFields({
      parameters: params,
      num: 0,
      post_data: {},
      response: {}
  })

  m.urlHandler.request = { context: context }
End Function

Function handleSetAssets()
  print "ImageCache::handleSetAssets"
  if m.top.assets <> invalid
    for i=1 to m.top.getField("maxCacheSize")
      assetId = m.top.assets[i].id
      print "ImageCache::handleSetAssets - assetId: "; assetId;
      loadAsset(assetId)
    end for
    print "ImageCache::handleSetAssets - assets found"
    'm.top.currentImageId = m.top.assets[1].id
  else
    print "ImageCache::handleSetAssets - assets not found"
  end if
end Function

Function handleImageResponse()
  print "ImageCache::handleImageResponse: "; m.urlHandler.response
  cache = m.cache
  response = m.urlHandler.response

  if response.code = 200
    ' If no asset is currently displayed, deplay the one that just loaded.
    if m.top.getField("currentImage") = ""
      m.top.setField("currentImage", response.file)
    else
      cache.unshift(response)
    end if

    ' Update the cache so any changes are reflected in the externally-facing field.
    updateCache()
  end if
End Function

Function handleSetCurrentImageId()
  print "ImageCache::handleSetCurrentImageId"

  currentImageId = m.top.getField("currentImageId")
  print "Looking for "; currentImageId; " in cache"
  print "Current cache: "; m.cache

  cachedAssetIndex = getCacheIndexByAssetId(currentImageId)
  print "ImageCache::handleSetCurrentImageId - cachedAssetIndex: "; cachedAssetIndex

  if cachedAssetIndex <> invalid
    cachedAsset = m.cache[cachedAssetIndex]
    print "ImageCache::handleSetCurrentImageId - cachedAsset found: "; cachedAsset
    m.top.setField("currentImage", cachedAsset.file)

    ' Get next random asset before removing the current one from the cache.
    ' This ensures that the same asset never appears in the cache at twice
    ' at the same time.
    if m.cache.Count() < m.top.getField("maxCacheSize")
      print "ImageCache::handleSetCurrentImageId - cache not full"
      nextRandomAsset = selectRandomUnloadedAsset()
      print "ImageCache::handleSetCurrentImageId - nextRandomAsset: "; nextRandomAsset
      loadAsset(nextRandomAsset.id)
    else
      print "ImageCache::handleSetCurrentImageId - cache full"
    end if

    m.cache.Delete(cachedAssetIndex)
  else
    print "ImageCache::handleSetCurrentImageId - cachedAsset not found"
  end if

  updateCache()
End Function

Function selectRandomUnloadedAsset()
  print "ImageCache::selectRandomUnloadedAsset"
  if m.top.assets <> invalid
    print "ImageCache::selectRandomUnloadedAsset - assets found"
    assetCount = m.top.assets.Count()
    randomIndex = Int(Rnd(assetCount) - 1)
    print "ImageCache::selectRandomUnloadedAsset - randomIndex: "; randomIndex
    nextAsset = m.top.assets[randomIndex]

    print "ImageCache::selectRandomUnloadedAsset - Checking cache for next asset: "; nextAsset

    if getCacheIndexByAssetId(nextAsset.assetId) <> invalid
      print "ImageCache::selectRandomUnloadedAsset - nextAsset already in cache, get a different one."
      return selectRandomUnloadedAsset()
    else
      print "ImageCache::selectRandomUnloadedAsset - nextAsset not in cache, use it."
      return nextAsset
    end if
  else
    print "ImageCache::selectRandomUnloadedAsset - assets not found.  Bailing."
  end if
end Function

Function getCacheIndexByAssetId(id)
  print "ImageCache::getCacheIndexByAssetId: id: "; id
  cacheSize = m.cache.Count()
  if cacheSize = 0
    print "ImageCache::getCacheIndexByAssetId - cache empty"
    return invalid
  end if

  for i = 0 to cacheSize - 1
    asset = m.cache[i]
    if asset.assetId = id
      print "ImageCache::getCacheIndexByAssetId - asset found"
      return i
    end if
  end for

  return invalid
end Function

Function updateCache()
  m.top.setField("cache", m.cache)
  m.top.setField("cacheSize", m.cache.Count())
end Function
