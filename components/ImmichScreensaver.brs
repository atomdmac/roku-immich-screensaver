Function init()
  print "ImmichScreensaver::init"
  m.timer = createObject("roSGNode", "Timer")
  m.timer.duration = getSlideDuration()
  m.timer.repeat = true
  m.timer.control = "stop"
  m.timer.observeField("fire", "showNextAsset")

  ' Configure album
  m.album = createObject("roSGNode", "Album")
  m.album.observeField("currentAlbum", "handleAlbumLoaded")

  ' Configure cache
  m.cache = createObject("roSGNode", "ImageCache")
  m.cache.observeField("currentImage", "handleImageChange")

  ' Configure inputs
  m.top.observeField("isReady", "handleReady")
End Function

Function handleReady()
  print "ImmichScreensaver::handleReady"

  ' TODO: Make apiKey and albumUrl configurable
  m.album.setField("apiKey", getApiKey())
  m.album.setField("albumUrl", getAlbumUrl())

  m.cache.setField("apiKey", getApiKey())
  m.cache.setField("baseAssetUrl", getBaseAssetUrl())

  ' poster = m.top.findNode("CurrentImage")
  ' poster.observeField("bitmapWidth", "handleChangePosterSize")
  ' poster.observeField("bitmapHeight", "handleChangePosterSize")
end Function

Function showNextAsset()
  print "ImmichScreensaver::showNextAsset"

  if m.cache.getField("cacheSize") > 0
    cachedAssets = m.cache.getField("cache")
    currentAssetId = m.cache.getField("currentImageId")

    print "cachedAssets: "; cachedAssets.count()

    nextCachedAssetId = cachedAssets[0].assetId

    ' HACK: Sometimes the first asset is the same as the current asset, so we
    ' need to skip it.  This is a hack, but it works.
    if nextCachedAssetId = currentAssetId
      nextCachedAssetId = cachedAssets[1].assetId
    end if

    print "nextCachedAssetId: "; nextCachedAssetId
    print "prevCachedAssetId: "; currentAssetId
    m.cache.setField("currentImageId", nextCachedAssetId)
  end if
End Function

Function handleAlbumLoaded()
  print "ImmichScreensaver::handleAlbumLoaded"
  assets = m.album.getField("currentAlbum").assets
  ' print "Adding"; assets.count(); "images to cache"
  ' print "Assets: "; assets
  m.cache.setField("assets", assets)
end Function

Function handleImageChange()
  print "ImmichScreensaver::handleImageChange"
  poster = m.top.findNode("ImageDisplay")
  poster.currentAssetPath = m.cache.getField("currentImage")
  m.timer.control = "start"
End Function

Function handleChangePosterSize()
  ' print "ImmichScreensaver::handleChangePosterSize"
  poster = m.top.findNode("CurrentImage")
  width = poster.bitmapWidth
  height = poster.bitmapHeight

  poster.translation=[(1920 - width) / 2, (1080 - height) / 2]

  ' TODO: Move animation trigger to a separate function
  m.top.findNode("transAnimation").control = "start"
end Function
