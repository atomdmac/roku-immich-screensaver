Function init()
  ' Track which asset display is currently active so we can switch between them
  ' true  = AssetA
  ' false = AssetB
  m.currentAsset = "AssetA"

  m.dimensions = [ 1920, 1080 ]

  m.top.observeField("currentAssetPath", "handleImageChanged")

  assetA = m.top.findNode("AssetA")
  assetA.observeField("loadStatus", "handleAssetLoaded")
  assetA.loadWidth = m.dimensions[0]
  assetA.loadHeight = m.dimensions[1]

  assetB = m.top.findNode("AssetB")
  assetB.observeField("loadStatus", "handleAssetLoaded")
  assetB.loadWidth = m.dimensions[0]
  assetB.loadHeight = m.dimensions[1]

  loadingTextAnimation = m.top.findNode("loadingTextAnimation")
  loadingTextAnimation.control = "start"

  ' DEBUG !!!!
  txt1 = m.top.findNode("debugText1")
  txt2 = m.top.findNode("debugText2")
  txt3 = m.top.findNode("debugText3")

EndFunction

Function handleImageChanged()
  nextPosterName = getNextPosterName()
  nextPoster = m.top.findNode(nextPosterName)
  nextPoster.opacity = 0
  nextPoster.uri = m.top.currentAssetPath
EndFunction

Function getBitmapDimensions(asset)
  return [ asset.bitmapWidth, asset.bitmapHeight ]
EndFunction

Function getDimensions(asset)
  return [ asset.width, asset.height ]
EndFunction

Function getLoadDimensions(asset)
  return [ asset.loadWidth, asset.loadHeight ]
EndFunction

Function getAssetOrientation(asset)
  dims = getBitmapDimensions(asset)
  if dims[0] > dims[1] then
    return "landscape"
  else
    return "portrait"
  end if
EndFunction

Function getPanAmounts(asset)
  displayDims = getScreenDimensions()
  currentDims = getBitmapDimensions(asset)
  return [
    -Abs(displayDims[0] - currentDims[0]),
    -Abs(displayDims[1] - currentDims[1])
  ]
EndFunction

Function getScreenDimensions()
  return m.dimensions
EndFunction

Function calculateZoomScaleDimensions(asset)
  displayDims = getScreenDimensions()
  bitmapDims = getBitmapDimensions(asset)
  if getAssetOrientation(asset) = "landscape" then
    return [
      (displayDims[1] / bitmapDims[1]) * bitmapDims[0],
      displayDims[1]
    ]
  else
    return [
      displayDims[0],
      (displayDims[0] / bitmapDims[0]) * bitmapDims[1]
    ]
  end if
EndFunction

Function getNextPosterName()
  if m.currentAsset = "AssetA" then
    return "AssetB"
  else
    return "AssetA"
  end if
EndFunction

Function doPanAnimation()
  nextPosterName = getNextPosterName()
  nextPoster = m.top.findNode(nextPosterName)
  ' Pan animations
  nextPan = m.top.findNode(nextPosterName + "Pan")
  nextPan.control = "start"
  ' NOTE: Set duration to 1sec longer than the duration of the slide switcher timer
  nextPan.duration = getSlideDuration() + getFadeAnimationDuration()
  nextPan.control = "start"
  nextPanInterp = m.top.findNode(nextPosterName + "PanInterp")
  panEnd = getPanAmounts(nextPoster)
  ' Pan across the middle 50% of the image since that is generally where the
  ' subject(s) of the image is.
  ' TODO: Make "pan amount" a configurable value
  nextPanInterp.keyValue = [
    [ panEnd[0] * 0.25, panEnd[1] * 0.25 ]
    [ panEnd[0] * 0.75, panEnd[1] * 0.75 ]
  ]
EndFunction

Function centerPosters()
  screenDims = getScreenDimensions()
  nextPosterName = getNextPosterName()
  nextPoster = m.top.findNode(nextPosterName)
  newX = ((screenDims[0] - nextPoster.bitmapWidth)  / 2)
  newY = ((screenDims[1] - nextPoster.bitmapHeight) / 2)
  nextPoster.translation = [ newX, newY ]
EndFunction

Function doFadeAnimation()
  nextPosterName = getNextPosterName()
  nextPoster = m.top.findNode(nextPosterName)

  ' Fade animations
  nextAnimationInterp = m.top.findNode(nextPosterName + "AnimationInterp")
  nextAnimationInterp.reverse = false

  nextAnimation = m.top.findNode(nextPosterName + "Animation")
  nextAnimation.control = "start"

  currentAnimationInterp =  m.top.findNode(m.currentAsset + "AnimationInterp")
  currentAnimationInterp.reverse = true

  currentAnimation = m.top.findNode(m.currentAsset + "Animation")
  currentAnimation.control = "start"
EndFunction

Function showDebugInfo()
  nextPosterName = getNextPosterName()
  nextPoster = m.top.findNode(nextPosterName)
  ' DEBUG !!!!
  ' Boy, is this hacky but it works for now.
  txt1 = m.top.findNode("debugText1")
  txt2 = m.top.findNode("debugText2")
  txt3 = m.top.findNode("debugText3")

  assetOrientation = getAssetOrientation(nextPoster)
  dims = getDimensions(nextPoster)
  bitmapDims = getBitmapDimensions(nextPoster)
  loadDims = getLoadDimensions(nextPoster)
  zoomDims = calculateZoomScaleDimensions(nextPoster)
  panAmounts = getPanAmounts(nextPoster)
  displayDims = getScreenDimensions()
  
  ' Debug Messages
  dimsStr  = "Dims: " + str(dims[0]) + "x" + str(dims[1])
  bitmapDimsStr  = "Bitmap: " + str(bitmapDims[0]) + "x" + str(bitmapDims[1])
  loadDimsStr = "Load: " + str(loadDims[0]) + "x" + str(loadDims[1])
  zoomDimsStr = "Scale (Calc'd): " + str(zoomDims[0]) + "x" + str(zoomDims[1])
  panAmountStr  = "Pan Amnt: " + str(panAmounts[0]) + "x" + str(panAmounts[1])
  displayDimsStr = "Display: " + str(displayDims[0]) + "x" + str(displayDims[1])

  ' Show scaling math
  ' inputStr1 = str(displayDims[0]) + " - " + str(zoomDims[0]) + " = " + str(displayDims[0] - zoomDims[0])
  ' inputStr2 = str(displayDims[1]) + " - " + str(zoomDims[1]) + " = " + str(displayDims[1] - zoomDims[1])

  ' diffStr = "Diff: " + inputStr1 + "x" + inputStr2 + " = " + inputResult

  txt1.text = ""
  txt2.text = ""
  txt3.text = ""
EndFunction

Function handleAssetLoaded()
  nextPosterName = getNextPosterName()

  ' We only care if the asset is loaded and ready.
  nextNode = m.top.findNode(nextPosterName)
  if nextNode.loadStatus <> "ready" then
    return invalid
  end if

  nextPoster = m.top.findNode(nextPosterName)

  doFadeAnimation()
  centerPosters()
  ' doPanAnimation()

  ' Toggle the current asset
  if m.currentAsset = "AssetA" then
    m.currentAsset = "AssetB"
  else
    m.currentAsset = "AssetA"
  end if

  ' showDebugInfo()
EndFunction
