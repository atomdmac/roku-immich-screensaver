' How long to display each slide, in seconds
Function getSlideDuration()
  return 15
EndFunction

' How long to fade between slides, in seconds
Function getFadeAnimationDuration()
  return 1
EndFunction

' The API key to use to access the Immich API
' TODO: Make API key configurable from UI
Function getApiKey()
  return "API_TOKEN_GOES_HERE"
EndFunction

' The base URL for the Immich API
Function getBaseApiUrl()
  ' TODO: Make API URL configurable from UI
  return "https://photos.atommac.com/api"
EndFunction

' The URL for the album to display
' TODO: Allow user to choose album from UI
' TODO: Make album ID configurable from UI
Function getAlbumUrl()
  return getBaseApiUrl() + "/albums/ALBUM_ID_GOES HERE"
EndFunction

' The URL for the asset to display
Function getBaseAssetUrl()
  return getBaseApiUrl() + "/assets"
EndFunction
