Function init()
  print "Album::init"

  m.urlHandler = CreateObject("roSGNode", "Content UrlHandler")
  m.urlHandler.observeField("response", "onAlbumResponse")

  ' Configure inputs
  m.top.observeField("albumUrl", "loadAlbum")
  m.top.observeField("apiKey", "loadAlbum")
End Function

Function loadAlbum()
  m.top.setField("albumLoaded", "false")

  print "top::loadAlbum"

  context = createObject("roSGNode", "Node")
  params = {
      headers: {
        "x-api-key": m.top.getField("apiKey")
      },
      uri: m.top.getField("albumUrl"),
      method: "GET",
      params: {}
      ' file: "tmp:/immich_album.json"
  }

  context.addFields({
      parameters: params,
      num: 0,
      post_data: {},
      response: {}
  })

  m.urlHandler.request = { context: context }
End Function

Function onAlbumResponse()
  if m.urlHandler.response <> invalid then
    if Len(m.urlHandler.response.content) <> 0 then
      m.top.setField("albumLoaded", "false")

      m.top.setField("currentAlbum", parseJson(m.urlHandler.response.content))
    end if
  end if
end function
