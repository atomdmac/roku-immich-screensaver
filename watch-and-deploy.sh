#!/bin/bash
BSC=$(which bsc)
if [[ -z "$BSC" ]]; then
  echo "The bsc compiler not found.  Quitting..."
  exit 1
fi

$BSC \
  --deploy \
  --host 192.168.1.152 \
  --username rokudev \
  --password rokudev \
  --watch
